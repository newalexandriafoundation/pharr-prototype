(ns app.ui
  (:require
   [clojure.set :as set]
   [clojure.string :as string]
   [reitit.core :as r]
   [rum.core :as rum]))

(rum/defc Page [opts children]
  (let [{:keys [scripts state title]
         :or {scripts ["/assets/cljs-out/dev-main.js"]
              state {}
              title "Pharr’s Homer"}} opts]
    [:html
     [:head
      [:meta {:http-equiv "Content-Type" :content "text/html; charset=UTF-8"}]
      [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0"}]
      #_[:link {:href "/feed.xml" :rel "alternate" :title "Pharr's Homer" :type "application/atom+xml"}]
      #_[:link {:href "/static/favicons/apple-touch-icon-152x152.png" :rel "apple-touch-icon-precomposed" :sizes "152x152"}]
      #_[:link {:href "/static/favicons/favicon-196x196.png" :rel "icon" :sizes "196x196"}]
      #_[:link {:href "/static/favicons/favicon-32x32.png" :rel "icon" :sizes "32x32"}]
      #_[:link {:rel "preload" :href "/static/favicons/apple-touch-icon-152x152.png" :as "image"}]

      [:title title]

      [:link {:href "/assets/css/tachyons.min.css"
              :rel "stylesheet" :type "text/css"}]]
     [:body
      [:header
       [:nav.border-box.db.dt-l.pa3.ph5-l.w-100
        [:a.db.dtc-l.v-mid.mid-gray.link.dim.w-100.w-25-l.tc.tl-l.mb2.mb0-l
         {:href "/" :title "Home"} "Pharr’s Homer"]
        [:div.db.dtc-l.v-mid.w-100.w-75-l.tc.tr-l
         [:a.link.dim.dark-gray.f6.f5-l.dib.mr3.mr4-l {:href "#" :title "Lessons"} "Lessons"]
         [:a.link.dim.dark-gray.f6.f5-l.dib {:href "#" :title "Account"} "Account"]]]]
      ;; because we put state in an atom here,
      ;; this component should only be used
      ;; when rendering HTML from the server
      [:main#app (children (atom state))]
      [:footer.pv4.ph3.ph5-m.ph6-l.mid-gray
       [:small.f6.db.tc "Copyright © 2021 New Alexandria Foundation"]
       [:div.tc.mt3
        [:a.f6.dib.ph2.link.mid-gray.dim {:href "#/language" :title "Language"} "Language"]
        [:a.f6.dib.ph2.link.mid-gray.dim {:href "#/privacy" :title "Privacy"} "Privacy policy"]
        [:a.f6.dib.ph2.link.mid-gray.dim {:href "#/terms" :title "Terms"} "Terms of use"]]]]
     [:script#initial-state {:type "application/edn"} (pr-str state)]
     (for [script scripts]
       [:script {:src script}])]))

(rum/defc Card [children]
  [:article.mw5.center.bg-white.br3.pa3.pa4-ns.mv3.ba.b--black-10 children])

(rum/defc Heading < rum/static [s]
  [:h1.f3.f2-m.f1-l.fw2.black-90.mv3 s])

(rum/defc Subheading < rum/static [s]
  [:h3.f5.f3-m.f2-l.fw2.black-80.mv3 s])

(rum/defc Home [props]
  (let [{:keys [match]} @props]
    [:div.tc.ph4
     (Heading "Learn Ancient Greek by Reading Homer")
     [:div.p4
      [:a.f6.link.dim.ba.bw1.ph3.pv2.mb2.dib.light-gray
       {:href "#/new/lesson"}
       "Create a new lesson"]
      [:a.f6.link.dim.ba.bw1.ph3.pv2.mb2.ml2.dib.dark-gray
       {:href "/lessons/3"}
       "Lesson 3"]]]))

(rum/defc WordButton [s on-click]
  [:a.f6.link.dim.br1.ba.ph3.pv2.mb2.dib.dark-gray.pointer
   {:on-click on-click} s])

(rum/defc CancelButton [s on-click]
  [:a.f6.link.dim.br1.ba.bw2.ph3.pv2.mb2.dib.white.bg-gray.pointer
   {:on-click on-click} s])

(rum/defc SubmitButton [s on-click]
  [:a.f6.link.dim.br1.ba.bw2.ph3.pv2.mb2.mr2.dib.white.bg-dark-green.pointer
   {:on-click on-click} s])

(defn format-word-bank [ws]
  (into [] (map-indexed (fn [idx itm] [idx itm false]) ws)))

(rum/defc TranslationSentence < rum/static
  [props]
  (let [{:keys [answers words sentence on-next]} props
        ;; I'm not sure that this is the most idiomatic
        ;; way to handle these updates, but we need
        ;; to be aware that words can be repeated,
        ;; so we can't rely on token uniqueness
        [translation set-translation!] (rum/use-state [])
        [word-bank set-word-bank!] (rum/use-state (format-word-bank words))
        [message set-message!] (rum/use-state nil)
        [success set-success!] (rum/use-state false)]
    (rum/use-effect! (fn []
                       (set-word-bank! (format-word-bank words))
                       (set-translation! [])) [sentence])
    [:div.tc "Translate:"
     [:h1.f3 sentence]
     [:p.pv1.f5.dark-red message]
     [:div.pb4
      (for [entry (sort-by #(last %) translation)
            :let [idx (first entry)
                  word (second entry)]]
        [:span.ma2 {:key (str word "-" idx)}
         (WordButton word (fn [_]
                            (set-message! nil)
                            (set-translation!
                             (mapv #(update % 2 dec)
                                   (remove #(-> % first (= idx)) translation)))
                            (set-word-bank! (assoc-in word-bank [idx 2] false))))])]
     (if-not success
       (rum/fragment
        [:div.pv4
         (SubmitButton
          "Check"
          (fn [_]
            (let [answer (string/join " " (map #(get % 1) translation))]
              (println answer answers)
              (if (some #(= % answer) answers)
                (set-success! true)
                (set-message! "Not quite. Try again!")))))
         (CancelButton
          "Clear"
          (fn [_]
            (set-translation! [])
            (set-word-bank! (mapv #(assoc % 2 false) word-bank))))]
        [:div.mw6.center
         (for [entry (remove #(last %) word-bank)
               :let [idx (first entry)
                     word (second entry)]]
           [:span.ma2 {:key (str word "-" idx)}
            (WordButton word (fn [_]
                               (set-message! nil)
                               (set-translation! (conj translation
                                                       (assoc entry 2
                                                              (count translation))))
                               (set-word-bank! (assoc-in word-bank [idx 2] true))))])])
       [:div.pv4
        [:p.pv2.f4.dark-green "✓ Nice work!"]
        (SubmitButton
         "Next ➔"
         (fn [_]
           (on-next)
           (set-success! false)
           (set-message! nil)))])]))

(rum/defc TranslationStudy < rum/static
  [init-exs]
  (let [[exs set-exs!] (rum/use-state init-exs)]
    (rum/use-effect! (fn [_] (set-exs! init-exs)) [init-exs])
    (if (> (count exs) 0)
      (TranslationSentence
       (merge (first exs)
              {:on-next (fn [_]
                          (set-exs! (rest exs)))}))
      [:div.tc
       [:p.pv1.f4.dark-green "All finished!"]
       (SubmitButton "Start over?" (fn [_] (set-exs! init-exs)))])))

(rum/defcs VocabCard < (rum/local false ::show-def?)
  [state word]
  (let [{:keys [definition headword on-finish]} word
        show-def? (::show-def? state)]
    (Card
     [:div.tc.pointer {:on-click
                       (fn [_]
                         (if @show-def?
                           (do
                             (reset! show-def? false)
                             (on-finish))
                           (reset! show-def? true)))}
      [:h1.f4 headword]
      [:hr.mw3.bb.bw1.b--black-10]
      (when @show-def? [:p.lh-copy.measure.center.f5.black-70 definition])])))

(rum/defcs VocabStudy < (rum/local #{} ::seen)
  [state m]
  (let [seen (::seen state)
        unseen (set/difference (set (shuffle (keys m))) @seen)
        w (first unseen)]
    [:div
     [:h2.f5.gray "Click on the card to show the definition. Click again to advance to the next card."]
     (VocabCard {:definition (string/join ", " (get-in m [w :en]))
                 :headword w
                 :on-finish (fn [_]
                              (swap! seen conj w)
                              (when (= (count @seen) (count m))
                                (reset! seen #{})))})]))

(rum/defc Lesson < rum/reactive [props]
  (let [{:keys [lesson match]} (rum/react props)
        {:keys [title subtitle
                vocab]} lesson]
    [:div.tc.ph4
     (Heading title)
     (Subheading subtitle)
     [:p
      [:a.f6.link.dim.ba.bw1.ph3.pv2.mb2.dib.dark-gray
       {:href "?study=vocab"} "Vocab"]
      [:a.f6.link.dim.ba.bw1.ph3.pv2.mb2.ml2.dib.dark-gray
       {:href "?study=grc->en"} "Greek to English"]
      [:a.f6.link.dim.ba.bw1.ph3.pv2.mb2.ml2.dib.dark-gray
       {:href "?study=en->grc"} "English to Greek"]]
     [:div
      (let [study (get-in match [:parameters :query :study])]
        (case study
          "vocab" (VocabStudy vocab)
          "grc->en" (TranslationStudy (:grc->en-exercises lesson))
          "en->grc" (TranslationStudy (:en->grc-exercises lesson))
          (VocabStudy vocab)))]]))

(rum/defc Label < rum/static [props s]
  [:label.f6.b.db.mb2 (or props {}) s])

(rum/defc Input < rum/static [type props]
  [:input.input-reset.ba.b--black-20.pa2.mb2.db.w-100
   (merge props {:type type})])

(rum/defc LessonForm [props]
  [:form.pa4.black-80
   [:div.measure
    (Label {:for "lesson-title"} "Title")
    (Input "text" {:id "lesson-title" :value (:title props)})]])

(rum/defc Root < rum/reactive [props]
  (let [{:keys [match]} (rum/react props)]
    (case (:name match)
      :app.routes/home (Home props)
      :app.routes/lesson (Lesson props)
      "No match")))
