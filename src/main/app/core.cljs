(ns app.core
  (:require
   [cljs.reader :as reader]
   [goog.string :as gstring]
   [reitit.coercion.schema :as rcs]
   [reitit.frontend :as rf]
   [reitit.frontend.controllers :as rfc]
   [reitit.frontend.easy :as rfe]
   [rum.core :as rum]
   [app.routes :refer [routes]]
   [app.state :refer [state]]
   [app.ui :as ui]))

(def router (rf/router routes {:data {:coercion rcs/coercion}}))

(defn initial-state []
  (reader/read-string
   (gstring/unescapeEntities
    (.-text (js/document.getElementById "initial-state")))))

(defn init! []
  (reset! state (initial-state))
  (rfe/start!
   router
   (fn [m]
     (let [{:keys [data parameters]} m]
       (swap!
        state
        update-in
        [:match]
        (fn [old-match]
          (when m
            (assoc
             m
             :controllers (rfc/apply-controllers
                           (:controllers old-match) m)
             :name (:name data)
             :parameters parameters))))))
   {:use-fragment false})
  (enable-console-print!)
  (rum/hydrate (ui/Root state) (js/document.getElementById "app")))

(init!)
