(ns app.api
  (:require
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [clojure.string :as string]))

(defonce lesson-resource "resources/public/edn/lessons.edn")
(defonce vocab-resource "resources/public/edn/vocab.edn")

(defn load-edn
  "Load edn from an io/reader source (filename or io/resource)."
  [source]
  (try
    (with-open [r (io/reader source)]
      (edn/read (java.io.PushbackReader. r)))
    (catch java.io.IOException e
      (printf "Couldn't open '%s': %s\n" source (.getMessage e)))
    (catch RuntimeException e
      (printf "Error parsing edn file '%s': %s\n" source (.getMessage e)))))

(defn vocab
  "Fetches vocab entries (gr->en) for the provided keywords.
  Usage: `(vocab [\"φίλη\" \"κλαγγή\"])`"
  ([] (load-edn vocab-resource))
  ([kws]
   (let [all (load-edn vocab-resource)]
     (select-keys all kws))))

(defn get-words [s]
  (string/split s #"\s"))

(defn required-word-freqs [answers]
  (apply merge-with #(max %1 %2)
         (map #(frequencies (get-words %)) answers)))

(defn make-exercises [raw-exs]
  (let [translations (->> (map rest raw-exs)
                          (flatten)
                          (string/join " "))
        all-words (set (get-words translations))]
    (map
     (fn [ex]
       (let [answers (rest ex)
             req-freqs (required-word-freqs answers)
             ex-words (flatten (map #(repeat (val %) (key %)) req-freqs))]
         {:sentence (first ex)
          :answers answers
          :words (shuffle (concat ex-words (take (+ 1 (rand-int 5))
                                                 (shuffle all-words))))}))
     raw-exs)))

(defn lesson [n]
  (let [lessons (load-edn lesson-resource)
        k (keyword (str "lesson" n))
        l (k lessons)
        l-vocab (vocab (:vocab l))
        {:keys [grc->en en->grc]} l]
    (assoc l
           :vocab l-vocab
           :grc->en-exercises (make-exercises grc->en)
           :en->grc-exercises (make-exercises en->grc))))

(comment
  (def l (lesson 3))
  (def grc->en (:grc->en l))
  (def ex (first grc->en))

  (def answers (rest ex))

  ;; TODO: Move these examples into a test

  (def translations (flatten (map rest grc->en)))

  (def translation-string (string/join " " translations))

  (def freqs
    #_[{"The" 1 "good" 1 "and" 1 "bad" 2 "plans" 1}
     {"The" 2 "plans" 1 "are" 1 "good" 7 "and" 1 "bad" 1}
     {"the" 3 "good" 2 "plans" 1 "are" 5 "bad" 2}]
    (map #(frequencies (get-words %)) answers))

  (apply merge-with #(max %1 %2) freqs)

  (make-exercises (:grc->en l)))
